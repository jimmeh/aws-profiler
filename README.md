# aws-profiler

Writes and reads aws cli profile credentials with a passphrase (openssl enc) files so secret keys are not stored in the clear on the file system.

By default, aws cli stores account credentials in a clear text file ~/.aws/credentials. This makes me itchy. My preference is for secret keys to be protected by a passphrase similar to the ssh approach. This simple bash script uses open-ssl enc to write encrypted aws credentials files and read the creds into shell env vars as required.

This script is NOT endorsed by or affiliated with AWS in any way. Use at your own risk. Nothing is guaranteed. The script was kept as minimal for quick review.

Installed OpenSSL package is required.


## Usage

### Write passphrase protected credentials

    $ aws-profiler -w <storename>

Will prompt for credentials and passphrase, and write creds protected by 'openssl enc' to ~/.aws-profiler/<storename>.enc
Default storename is aws-profile


### Read credentials into env vars

    $ bash
    $ source awsprofiler -r <storeename>

Exports AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY, and AWS_DEFAULT_REGION vars with creds from encrypted storename file.
The storename of the loaded creds will be introduced into env var $AWSPROFILER
and can be displayed as below


### Show creds profile storename

    $ aws-profiler

Will display current storename of profile creds in env vars





## Create AWS credentials secrets
https://docs.aws.amazon.com/IAM/latest/UserGuide/id_credentials_access-keys.html
AWS > Web console > top menubar, right hand side: username@account > Security Credentials
  AWS IAM credentials > Create access key button
