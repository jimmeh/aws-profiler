#!/bin/bash
set -e; set -u; set -o pipefail
readonly SCRNAME=$( basename $0 )
readonly SCRDIR=$( cd $( dirname $0 ) ; pwd -P )

readonly -a VARS=(AWS_ACCESS_KEY_ID AWS_SECRET_ACCESS_KEY AWS_DEFAULT_REGION)
readonly DIR="$HOME/.aws-profiler"


usage() {
  cat <<EOS
Writes and reads encrypted aws profile credentials into env vars.

Show creds profile storename
$ $SCRNAME        Will display current storename of profile creds in env vars

Store credentials
$ $SCRNAME -w <storename>   Will prompt for credentials and passphrase

Read credentials into env vars
$ bash
$ source $SCRNAME -r <storeename>   Populates env vars with aws creds from file

Store names represent encrypted files are stored in $DIR
Default storename is aws-profile
Storename will be introduced into env var \$AWSPROFILER

https://docs.aws.amazon.com/IAM/latest/UserGuide/id_credentials_access-keys.html
AWS > Web console > top menubar, right hand side: username@account > Security Credentials
  AWS IAM credentials > Create access key button
EOS
}


if ! [ -d "$DIR" ] ; then
  mkdir -m u=rwx,go= "$DIR"
  echo "Encrypted aws credentials maintained by $SCRNAME" > "$DIR/readme.txt"
fi


mode='show'
storename='aws-profile'

OPTIND=1
while getopts "hwr" opt; do
  case "$opt" in
    h) usage; exit 0 ;;
    w) mode='write' ;;
    r) mode='read' ;;
    ?) usage; exit 1 ;;
  esac
done
shift "$((OPTIND-1))"
args=()
for arg; do args+=("${arg}"); done

if [ ${#args[@]} -gt 1 ]
then
  echo "Only 1 arg (storename) accepted" 
  usage ; exit 1
fi

if [ ${#args[@]} -eq 1 ]
then
  storename="${args[0]}"
fi


isvar () {
  local seeking="$1"; shift
  local exitcode=1
  for element in ${VARS[@]}; do
    if [[ "$element" == "$seeking" ]]; then
      exitcode=0
      break
    fi
  done
  return $exitcode
}



main () {
  storefile="${DIR}/${storename}.enc"
  case "$mode" in
    show) 
      p='not loaded'
      if [ -v AWSPROFILER ] ; then p="$AWSPROFILER" ; fi
      echo "aws profiler: $p"
      if ! [ -v AWSPROFILER ] ; then echo "Did you run -r as a subshell? Use source" ; fi
      ;;

    write)     
      instr=''
      for v in ${VARS[@]} ; do
        echo "Please enter value for $v (a-zA-Z0-9):"
        read line
        instr+="${v}=${line}"$'\n'
      done
      echo 
      echo "Will overwrite ${storefile} That ok? (y/n)"
      read line
      if [[ "$line" != 'y' ]]; then
        echo "Nothing written"
        exit 0
      fi
      echo "$instr" | openssl enc -aes-256-cbc -salt -pbkdf2 -out "$storefile"
      echo "$storename creds written to $storefile"
      ;;

    read)
      if ! [ -f "${storefile}" ]; then
        echo "${storename} doesn't have a file. Expected $storefile" ; echo "-h for help"
        exit 1
      fi
      instr="$( openssl enc -d -aes-256-cbc -pbkdf2 -in $storefile )"
      declare -A assoc
      while read -r line
      do 
        vname=$( echo $line | cut -f1 -d= )
        vvalue=$( echo $line | cut -f2 -d= )
        if isvar $vname
        then
          echo "Setting ${vname}"
          assoc[$vname]="$vvalue"
        fi
      done <<< "$instr"
      export AWS_ACCESS_KEY_ID="${assoc[AWS_ACCESS_KEY_ID]}"
      export AWS_SECRET_ACCESS_KEY="${assoc[AWS_SECRET_ACCESS_KEY]}"
      export AWS_DEFAULT_REGION="${assoc[AWS_DEFAULT_REGION]}"
      echo "${storename} is set"
      export AWSPROFILER="$storename"
      ;;
  esac
}

main
